package com.google.zxing;

public class ZxingConstant {
    public static final int WHAT_DECODE = 600001;
    public static final int WHAT_DECODE_FAILED = 600002;
    public static final int WHAT_DECODE_SUCCEEDED = 600003;
    public static final int WHAT_LUNCH_PRODUCT_QUERY = 600004;
    public static final int WHAT_QUIT = 600005;
    public static final int WHAT_RESTART_PREVIEW = 600006;
    public static final int WHAT_RETURN_SCAN_RESULT = 600007;

}
