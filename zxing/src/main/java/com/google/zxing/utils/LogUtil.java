package com.google.zxing.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Log工具
 * author fpp
 * date 2019/3/26 14:38
 */
public class LogUtil {

    private static final String TAG = "zxing";

    public static boolean isDebug = true;// 是否需要打印bug，可以在application的onCreate函数里面初始化

    @SuppressLint("StaticFieldLeak")
    private static LogUtil INSTANCE = null;
    private int mPId;
    private String pkgName = "pkgName";

    private LogUtil() {
        /* cannot be instantiated */
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    private LogUtil(Context context) {
        init(context);
        mPId = android.os.Process.myPid();
    }

    public static LogUtil getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LogUtil(context);
        }
        return INSTANCE;
    }

    // 下面四个是默认tag的函数
    public static void i(String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.i(TAG, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    public static void d(String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.d(TAG, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    public static void e(String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.e(TAG, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    public static void v(String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.v(TAG, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }
    public static void w(String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.w(TAG, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    // 下面是传入自定义tag的函数
    public static void i(String tag, String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.i(tag, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    public static void d(String tag, String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.d(tag, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    public static void e(String tag, String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.e(tag, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    public static void v(String tag, String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.v(tag, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }
    public static void w(String tag, String msg) {
        if (isDebug) {
            StackTraceElement targetStackTraceElement = getTargetStackTraceElement();
            Log.w(tag, msg + " (" + targetStackTraceElement.getFileName() + ":"
                    + targetStackTraceElement.getLineNumber() + ")");
        }
    }

    /**
     * 获取LogUtil的使用位置信息
     *
     * @return LogUtil的使用位置信息
     */
    private static StackTraceElement getTargetStackTraceElement() {
        // find the target invoked method
        StackTraceElement targetStackTrace = null;
        boolean shouldTrace = false;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            boolean isLogMethod = stackTraceElement.getClassName().equals(LogUtil.class.getName());
            if (shouldTrace && !isLogMethod) {
                targetStackTrace = stackTraceElement;
                break;
            }
            shouldTrace = isLogMethod;
        }
        return targetStackTrace;
    }

    /**
     * 初始化目录
     *
     * @param context context
     */
    private void init(Context context) {
    }

    private String getFileName() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        return timeFormat.format(new Date(System.currentTimeMillis()));
    }


}