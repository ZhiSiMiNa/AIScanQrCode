package com.example.aiscanqrcode;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView pic;
    TextView showTxt;
    TextView tvAtyScanQrContent;

    private HashMap hashMap; //用hasmap放置二维码的参数
    private Bitmap bitmap;//声明一个bitmap对象用于放置图片;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getPermissions();
        initView();//初始化控件(这里用的是插件LayoutCreator)
    }

    private void initView() {
        hashMap = new HashMap();
        pic = findViewById(R.id.iv_aty_scan_pic);
        showTxt = findViewById(R.id.tv_aty_scan_show_text);
        tvAtyScanQrContent = findViewById(R.id.tv_aty_scan_qr_content);
        Button btnAtyScanCreate = findViewById(R.id.btn_aty_scan_create);
        Button btnAtyScanScanQr = findViewById(R.id.btn_aty_scan_scan_qr);
        Button btnAtyScanShow = findViewById(R.id.btn_aty_scan_show);
        btnAtyScanCreate.setOnClickListener(this);
        btnAtyScanScanQr.setOnClickListener(this);
        btnAtyScanShow.setOnClickListener(this);
        tvAtyScanQrContent.setOnClickListener(this);


    }

    private void create_QR_code() {
        hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        //定义二维码的纠错级别，为L
        hashMap.put(EncodeHintType.CHARACTER_SET, "utf-8");
        //设置字符编码为utf-8
        hashMap.put(EncodeHintType.MARGIN, 2);
        //设置margin属性为2,也可以不设置
        String contents = "最简单的Demo"; //定义二维码的内容
        BitMatrix bitMatrix = null;   //这个类是用来描述二维码的,可以看做是个布尔类型的数组
        try {
            bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, 250, 250, hashMap);
            //调用encode()方法,第一次参数是二维码的内容，第二个参数是生二维码的类型，第三个参数是width，第四个参数是height，最后一个参数是hints属性
        } catch (WriterException e) {
            e.printStackTrace();
        }

        int width = bitMatrix.getWidth();//获取width
        int height = bitMatrix.getHeight();//获取height
        int[] pixels = new int[width * height]; //创建一个新的数组,大小是width*height
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                //通过两层循环,为二维码设置颜色
                if (bitMatrix.get(i, j)) {
                    pixels[i * width + j] = Color.BLACK;  //设置为黑色

                } else {
                    pixels[i * width + j] = Color.WHITE; //设置为白色
                }
            }
        }

        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        //调用Bitmap的createBitmap()，第一个参数是width,第二个参数是height,最后一个是config配置，可以设置成RGB_565
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        //调用setPixels(),第一个参数就是上面的那个数组，偏移为0，x,y也都可为0，根据实际需求来,最后是width ,和height
        pic.setImageBitmap(bitmap);
        //调用setImageBitmap()方法，将二维码设置到imageview控件里

    }

    private void show_QR_code() {
        hashMap.put(DecodeHintType.CHARACTER_SET, "utf-8");//设置解码的字符，为utf-8
        int width = bitmap.getWidth();//现在是从那个bitmap中得到width和height
        int height = bitmap.getHeight();
        int[] pixels = new int[width * height];//新建数组，大小为width*height
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height); //和什么的setPixels()方法对应
        Result result = null;//Result类主要是用于保存展示二维码的内容的
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new RGBLuminanceSource(width, height, pixels)));
        //BinaryBitmap这个类是用于反转二维码的，HybridBinarizer这个类是zxing在对图像进行二值化算法的一个类
        try {
            result = new MultiFormatReader().decode(binaryBitmap);//调用MultiFormatReader()方法的decode()，传入参数就是上面用的反转二维码的
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        showTxt.setText(result.toString());//设置文字

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_aty_scan_create:
                create_QR_code(); //此方法用于创建二维码
                break;
            case R.id.btn_aty_scan_scan_qr:
                //打开扫描界面扫描条形码或二维码
//                Intent openCameraIntent = new Intent(MainActivity.this, CaptureActivity.class);
                Intent openCameraIntent = new Intent(MainActivity.this, PayActivity.class);
//                openCameraIntent.putExtra(Intents.Scan.CAMERA_ID, Camera.CameraInfo.CAMERA_FACING_FRONT);
                startActivityForResult(openCameraIntent, 0);
                break;
            case R.id.btn_aty_scan_show:
                show_QR_code();//此方法用于显示二维码的内容
                break;
            case R.id.tv_aty_scan_qr_content:
                Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 0);
                break;
        }
    }


    //扫描二维码，返回的结果
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
            Toast.makeText(MainActivity.this, "扫码结果：" + scanResult, Toast.LENGTH_LONG).show();
            tvAtyScanQrContent.setText(scanResult);
        }
    }

    private void getPermissions() {
        List<String> perms = new ArrayList<>();
        perms.add(Manifest.permission.INTERNET);
        perms.add(Manifest.permission.ACCESS_NETWORK_STATE);
        perms.add(Manifest.permission.CHANGE_NETWORK_STATE);
        perms.add(Manifest.permission.ACCESS_WIFI_STATE);
        perms.add(Manifest.permission.CHANGE_WIFI_STATE);
        perms.add(Manifest.permission.BLUETOOTH);
        perms.add(Manifest.permission.BLUETOOTH_ADMIN);
        perms.add(Manifest.permission.RECORD_AUDIO);
        perms.add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
        perms.add(Manifest.permission.CAMERA);
        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        perms.add(Manifest.permission.ACCESS_FINE_LOCATION);
        perms.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        perms.add(Manifest.permission.READ_PHONE_STATE);
        List<String> permsNot = new ArrayList<>();
        for (int i = 0; i < perms.size(); i++) {
            int per = ContextCompat.checkSelfPermission(this, perms.get(i));
            if (per != PERMISSION_GRANTED) {
                permsNot.add(perms.get(i));
            }
        }

        if (permsNot.size() > 0) {
            String[] permissions = new String[permsNot.size()];
            permsNot.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 1);
        } else {
            Log.e("", "request permissions success");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (hasAllPermissionsGranted(grantResults)) {
                Log.e("", "request permissions success");
            }
        }
    }

    /**
     * 是否授权所有权限
     *
     * @param grantResults 授权响应集合
     * @return 响应
     */
    private boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }
}